.PHONY: all
all: build/netctl

build:
	mkdir -p build

build/netctl: netctl/netctl.go | build
	go build -o $@ $^
