TFTP
====

This role sets up a host node with a TFTP server. Files are served from

```
/var/lib/tftpboot
```

This location is a Fedora convention.

After this role has been executed, the tftp server is running, enabled in `systemd` and will start with each boot. The implementation used is Fedora's packaging of [tftp-hpa](https://mirrors.edge.kernel.org/pub/software/network/tftp/).

