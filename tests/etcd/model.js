// etcd 3 node testing topology

topo = {
  name: 'etcd',
  nodes: [
    fedora('client'),
    fedora('db0'),
    fedora('db1'),
    fedora('db2')
  ],
  switches: [
    cumulus('sw')
  ],
  links: [
    Link('client', 0, 'sw', 0),
    Link('db0', 0, 'sw', 1),
    Link('db1', 0, 'sw', 2),
    Link('db2', 0, 'sw', 3)
  ]
};

function cumulus(name) {
  return {
    name: name,
    image: 'cumulusvx-3.5-mvrf',
    cpu: { cores: 2 },
    memory: { capacity: MB(512) }
  }
}

function fedora(name) {
  return {
    name: name,
    image: 'fedora-28',
    cpu: { cores: 2 },
    memory: { capacity: GB(2) },
    mounts: [
      { source: env.PWD+'/../..', point: '/tmp/src' }
    ]
  }
}


