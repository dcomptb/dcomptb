// tftp 2 node testing topology

topo = {
  name: 'tftp',
  nodes: [
    fedora('client'),
    fedora('server'),
  ],
  switches: [
    cumulus('sw')
  ],
  links: [
    Link('client', 1, 'sw', 1),
    Link('server', 1, 'sw', 2)
  ]
}

function cumulus(name) {
  return {
    name: name,
    image: 'cumulusvx-3.5-mvrf',
    cpu: { cores: 2 },
    memory: { capacity: MB(512) }
  }
}

function fedora(name) {
  return {
    name: name,
    image: 'fedora-28',
    cpu: { cores: 2 },
    memory: { capacity: GB(2) },
    mounts: [
      { source: env.PWD+'/../..', point: '/tmp/src' }
    ]
  }
}


