package main

import (
	"github.com/spf13/cobra"
	"log"
)

func main() {

	log.SetFlags(0)

	rootCmd := &cobra.Command{
		Use:   "netctl",
		Short: "Control DCompTB networks",
	}

	/* infrastructure networks */
	infraCmd := &cobra.Command{
		Use:   "infra",
		Short: "Control infrastructure networks",
	}
	rootCmd.AddCommand(infraCmd)

	idnsCmd := &cobra.Command{
		Use:   "dns",
		Short: "Control infrastructure dns",
		Run:   idns,
	}
	infraCmd.AddCommand(idnsCmd)

	idhcpCmd := &cobra.Command{
		Use:   "dhcp",
		Short: "Control infrastructure dhcp",
		Run:   idhcp,
	}
	infraCmd.AddCommand(idhcpCmd)

	/* experiment control networks */
	xpCmd := &cobra.Command{
		Use:   " xp",
		Short: "Control experiment control networks",
	}
	rootCmd.AddCommand(xpCmd)

	xdnsCmd := &cobra.Command{
		Use:   "dns",
		Short: "Control experiment control net dns",
		Run:   xdns,
	}
	xpCmd.AddCommand(xdnsCmd)

	xdhcpCmd := &cobra.Command{
		Use:   "dhcp",
		Short: "Control experiment control net dhcp",
		Run:   xdhcp,
	}
	xpCmd.AddCommand(xdhcpCmd)

}

func idns(cmd *cobra.Command, args []string) {
}

func idhcp(cmd *cobra.Command, args []string) {
}

func xdns(cmd *cobra.Command, args []string) {
}

func xdhcp(cmd *cobra.Command, args []string) {
}
